<?php
	require_once 'mysqlConnection.php';
?>

<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@400;500;700&family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link
    rel="stylesheet"
    href="./assets/swiper/swiper-bundle.min.css"
  />
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79249620-1"></script>
  <script src="./assets/tailwindcss.js"></script>
  <script src="./assets/swiper/swiper-bundle.min.js"></script>
  <script type="text/javascript">

  	window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-79249620-1');

  	tailwind.config = {
      theme: {
        extend: {
          colors: {
            blue :{
              900 : '#131E44'
            },
            red :{
              100 : '#FFEBED',
              900 : '#9F2937'
            },
            slate :{
              50 : '#FFEBEF',
              300 : '#E6E6E6'
            },
			green :{
				50 : '#c0d4d3',
				100 : '#2d8984',
			}
          }
        },
        fontFamily:{
        	'display' : ['"Abhaya Libre"', 'serif'],
        	'body': ['"Montserrat"', 'sans-serif'],
        },
        screens: {
		  '2xs': '300px',
          'xs': '600px',
          'sm': '768px',
          'md': '960px',
          'lg': '1024px',
          'xl': '1280px',
          '2xl': '1536px',
        }
      }
    }
  </script>

  <!-- Facebook Pixel Code -->
  <script>
    !function (f, b, e, v, n, t, s) {
      if (f.fbq) return; n = f.fbq = function () {
        n.callMethod ?
        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
      n.queue = []; t = b.createElement(e); t.async = !0;
      t.src = v; s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '761598857328139');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=761598857328139&ev=PageView&noscript=1" /></noscript>

  <style type="text/tailwindcss">
    @layer utilities {
      .swiper-pagination-bullet {
        width: 15px;
        height: 15px;
      }

      .swiper-button-next::after, .swiper-button-prev::after{
        font-family : unset;
        content: none;
      }
    }
  </style>

  <title>S1 Kedokteran Gigi</title>
</head>
<body class="font-body overflow-x-hidden">
  <section>
  <div class="w-screen md:h-screen bg-white bg-contain bg-right-bottom bg-no-repeat lg:bg-[url('./assets/img/asset-1.png')] md:overflow-hidden">
  	<!-- <div class="w-screen md:h-screen bg-white bg-contain bg-right-bottom bg-no-repeat bg-[url('./assets/img/asset-6.png')] md:overflow-hidden"> -->

	  <div class="md:flex relative">
	  		<div class="md:flex-auto md:w-1/2 ">
	  			<div class="content-top md:relative md:flex md:flex-col md:h-screen md:justify-center">
	  				<div class="logo-wrapper text-center md:text-left md:absolute md:top-0 md:w-64 lg:w-auto py-10 px-16">
	  					<a href="https://iik.ac.id">
	  						<img class="inline" src="./assets/img/logo.png" />
	  					</a>
	  				</div>
	  				<div class="w-full md:w-[50%] mb-5 px-10 md:mb-10 md:px-16">
					  <h1 class="text-3xl md:text-3xl 2xs:text-xl lg:text-4xl text-center md:text-left font-bold text-blue-900 leading-none 2xs:px-8">Ribuan Alumni Dokter Gigi IIK Bhakta Sukses Mengabdi untuk Indonesia Hingga ke Pelosok Negeri.<br/><br/>
						Kini Giliranmu Menjadi Dokter Gigi Profesional Bersama IIK Bhakta</h1>
	  				</div>
	          <div class="reward-wrapper flex justify-center md:justify-start items-center  md:absolute md:bottom-0 py-5 lg:py-10 px-16 hidden md:flex">
	            <div class="w-56 lg:w-64">
	              <img class="mx-auto" src="./assets/img/rw-1.png" />
	            </div>
	            <div class="w-32 px-3">
	              <img class="mx-auto" src="./assets/img/rw-2.png" />
	            </div>
	            <div class="w-32 px-3">
	              <img class="mx-auto " src="./assets/img/rw-3.png" />
	            </div>
	          </div>
	  			</div>
	  		</div>
				<div class="md:flex-auto md:w-1/2 md:hidden">
					<img class="w-auto" src="./assets/img/asset-1.png" />
					<div class="reward-wrapper flex justify-center md:justify-start items-center  md:absolute md:bottom-0 py-5 lg:py-10 px-16">
						<div class="w-56 lg:w-64">
							<img class="mx-auto" src="./assets/img/rw-1.png" />
						</div>
						<div class="w-32 px-3">
							<img class="mx-auto" src="./assets/img/rw-2.png" />
						</div>
						<div class="w-32 px-3">
							<img class="mx-auto " src="./assets/img/rw-3.png" />
						</div>
					</div>
				</div>
	  	</div>
	  </div>

	  <div class="w-screen h-screen 2xs:h-auto bg-gray-200 py-6">
	  	<div class="md:flex relative sm:py-20  px-16 ">
	  		<div class="md:block md:flex-auto md:w-3/6 ">
				<div class="content-top relative flex sm:block sm:h-auto md:flex flex-col md:flex-col md:h-screen justify-center md:justify-center">
					<span class="sm:text-5xl 2xs:text-3xl text-green-100 2xs:pb-8 justify-center md:justify-center">
						Indonesia Butuh Anda untuk Kembangkan Kedokteran Gigi
					</span>
					<span class="text-xl text-green-100 font-bold sm:pt-10 sm:pb-4">Did You Know?</span>
					<span class="sm:w-4/6">Dalam setahun, lulusan dokter gigi dari 32 fakultas di Indonesia hanya 1.500 dokter gigi. Itu pun belum bisa memenuhi kebutuhan 267 juta jiwa di Indonedia. Indonesia masih sangat kekurangan dokter gigi. Idealnya sesuai perhitungan WHO, satu dokter gigi melayani 7.500 penduduk di Indonesia, sedangkan realitanya satu dokter gigi melayani 9.000 penduduk ditambah lagi persebaran dokter gigi di Indonesia juga belum merata, khususnya di wilayah Indonesia Timur.</span>
				</div>
		    </div>
		    <div class="md:flex-auto md:w-4/6 ">
		        <div class="content-top relative flex sm:block sm:h-auto md:flex flex-col md:flex-col justify-center md:justify-center">
		          	<div class="w-full sm:mb-16 md:mb-10 sm:px-5 md:pl-16 sm:py-8">
		              <img class="mx-auto sm:w-[80%]" src="./assets/img/asset-diagram.png" />
		            </div>
		        </div>
		    </div>
	  	</div>
	  </div>

	  <div class="w-screen h-auto md:h-auto 2xs:h-auto bg-gray-300 md:overflow-hidden">
	    <div class="md:flex relative 2xs:py-16 md:pt-16 ">
	      <div class="md:flex-auto md:w-3/5 ">
	        <div class="content-top relative flex 2xs:block 2xs:h-auto md:flex flex-col md:flex-col h-screen justify-center md:justify-center">
	          <div class="w-full mb-5 md:mb-10 px-5 md:pl-16">
	              	<p class="font-bold text-4xl">
	                	Apa Kata Kaprodi tentang Prodi S1 Kedokteran Gigi?
					</p>
					<p class="py-10">
						"Asyiknya kuliah di prodi ini, selain belajar teori, mahasiswa juga akan banyak melakukan praktik. Mahasiswa akan dihujani dengan banyaknya skill lab dan praktikum yang beragam."
					</p>
					<p class="font-bold text-xl">
	                	Mengapa harus memilih prodi ini?
					</p>
					<p class="pb-5">
						Dalam prodi S1 Kedokteran Gigi tidak hanya melingkupi kesehatan gigi saja, tetapi juga mencakup estetika gigi dan mulut. Selain itu, dokter gigi juga memiliki peran pada pengobatan lain seperti kanker mulut, merawat dan mempromosikan kesehatan mulut, kerja gigi dan lidah, ludah, serta kesadaran untuk menjaga kesehatan mulut. Asyiknya kuliah di prodi ini, selain belajar teori, mahasiswa juga akan banyak melakukan praktik. Mahasiswa akan dihujani dengan banyaknya skill lab dan praktikum yang beragam jenisnya. Hal ini wajar karena dokter gigi erat sekali kaitannya dengan keterampilan tangan. Mahasiswa juga mendapat pengetahuan tambahan tentang pengobatan tradisional dimana mahasiswa akan mempelajari  kekayaan alam sekitar yang sangat bermanfaat dan belum dieksplor serta berpotensi dikembangkan.
					</p>
					<div class="sm:flex rounded-lg border-4 border-green-100">
						<div class="sm:flex-auto sm:w-3/6 2xs:w-full 2xs:pl-5 py-6 ">
							<p class="font-bold">Tersedia</p>
							<ul class="list-disc pl-5">
								<li>Rumah Sakit Gigi dan Mulut IIK Bhakta</li>
								<li>115 Dental unit & Panaromic Cephalometric</li>
								<li>Dokter Gigi Spesialis</li>
								<li>Ruang IGD</li>
								<li>Layanan Swab Drive Thru</li>
							</ul>
						</div>
						<div class="sm:flex-auto sm:w-3/6 2xs:w-full bg-green-100 text-white 2xs:pl-5 py-6">
							<p class="font-bold">Tersedia Juga</p>
							<ul class="list-disc pl-5">
								<li>80 Dental Simulator</li>
								<li>Program Pendidikan Profesi Dokter Gigi</li>
							</ul>
						</div>
					</div>
	            </div>
	        </div>
	      </div>
	      <div class="md:block md:flex-auto md:w-2/5 ">
	        <div class="img-contet flex flex-auto justify-center">
	              <img class="w-[65%]"  src="./assets/img/asset-9.png" />
	        </div>
			<div class="rounded-2xl bg-green-100 px-5 py-3 text-white sm:w-[60%] 2xs:w-[80%]  m-auto text-center">
				<p class="font-bold">Nikmatus Sa'adah, drg., M.Si</p>
				<p>Kaprodi S1 Kedokteran Gigi</p>
			</div>
	      </div>
	    </div>
	  </div>

	  <div class="w-screen">
	    <div class="title-wrapper p-10">
	      <h3 class="text-3xl text-center font-bold">Testimoni Mahasiswa dan Alumni</h3>
	    </div>
	    <div class="px-5 md:px-28 relative">
	      <div class="swiper mySwiper">
	        <div class="swiper-wrapper">
	          <?php for($i=1; $i <= 3; $i++){ ?>
	          <div class="swiper-slide">
	            <div class="w-full">
	              <a href="javascript:;">
	                <img src="./assets/img/slide-<?= $i; ?>.jpg">
	              </a>
	            </div>
	          </div>
	        <?php } ?>
	        </div>
	      </div>
	      <div class="swiper-button-next w-16 right-10 hidden md:block">
	        <img src="./assets/img/next.png" />
	      </div>
	      <div class="swiper-button-prev w-16 left-10 hidden md:block">
	        <img src="./assets/img/prev.png" />
	      </div>
	      <div class="swiper-pagination relative mt-8"></div>
	    </div>
	    <div class="title-wrapper py-10 px-2 md:px-28">
	      <h3 class="text-3xl text-center font-bold border-t-2 pt-16">Kampus Inovatif dan Modern</h3>
	    </div>
	    <div class="content-wrapper px-5 md:px-24 pb-28">
	      <div class="flex flex-col  sm:flex-row">
	        <div class="sm:flex-1 w-full sm:w-1/3">
	          <div class="content-background mb-5 sm:mb-0 relative sm:px-5">
	            <img class="w-full" src="./assets/img/asset-12.png" />
	            <div class="z-10 absolute bottom-5 left-0 w-full">
	              <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">Support Mahasiswa untuk Selalu Berkarya dan Berprestasi</p>
	            </div>
	          </div>
	        </div>
	        <div class="sm:flex-1 w-full mb-5 sm:mb-0 sm:w-1/3">
	          <div class="content-background relative sm:px-5">
	            <img class="w-full" src="./assets/img/asset-13.png" />
	            <div class="z-10 absolute bottom-5 left-0 w-full">
	              <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">
	                Aktif Jalin Kerjasama dan Siap Menjadi Garda Terdepan dalam Kesehatan
	              </p>
	            </div>
	          </div>
	        </div>
	        <div class="sm:flex-1 w-full mb-5 sm:mb-0 sm:w-1/3">
	          <div  class="content-background relative sm:px-5">
	            <img class="w-full" src="./assets/img/asset-14.png" />
	            <div class="z-10 absolute bottom-5 left-0 w-full">
	              <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">
	                Selain Kuliah, Mahasiswa IIK Bhakta juga aktif dalam sosial media dengan konten inspiratif
	              </p>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="w-screen bg-zinc-300 bg-cover bg-center bg-no-repeat bg-[url('./assets/img/bg-section-3.png')]">
	  	<div class="content-wrapper px-5 sm:px-24">
	  		<div class="md:flex md:flex-row">
	  			<div class=" hidden lg:block pt-16 flex-auto w-2/6 pb-0">
	  				<div class="img-wrapper ">
	  					<img class="w-full " src="./assets/img/asset-15.png" />
	  				</div>
	  			</div>
	  			<div class="md:flex-auto md:w-4/6 md:pt-20 2xs:py-20 xl:pb-0 md-py-0">
	  				<div class="content text-white text-[24px] sm:text-2xl  md:text-3xl text-center md:text-left relative md:top-[50%] md:transform md:translate-y-[-50%] md:pl-16">
	  					<p class="mb-5"><strong>Tunggu Apalagi ?</strong></p>
	  					<p class="mb-5">Bergabunglah sekarang menjadi bagian<br class=" hidden sm:block" /> dari Keluarga Besar IIK Bhakti Wiyata untuk</p>
	  					<p class="mb-5"><strong>"Maju membangun dunia yang lebih sehat!"</strong></p>
	  					<a href="https://iik.ac.id/pendaftaran" class="bg-red-900 rounded-full px-3 sm:px-8 py-3 text-sm sm:text-lg" target="_blank" >Form Pendaftaran Online</a>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	  <div class="w-screen bg-blue-900 overflow-hidden relative before:bg-contain before:absolute before:left-2 lg:before:left-16 before:bottom-0 lg:before:bottom-[-50px] before:opacity-30 lg:before:opacity-100 before:w-40 lg:before:w-64 before:h-32 lg:before:h-48 before:content-[''] before:bg-no-repeat before:bg-center before:bg-[url('./assets/img/bg-section-4.png')] after:bg-contain after:absolute after:right-2 lg:after:right-16 after:top-10 lg:after:top-0 lg:after:w-64 lg:after:h-48 after:w-40 after:h-32 after:content-[''] after:bg-no-repeat after:bg-top bg-[url('./assets/img/bg-section-5.png')] after:opacity-30 lg:after:opacity-100">
	  	<div class="content-wrapper px-5 py-8 sm:px-24">
	  		<h3 class="font-display font-bold italic text-xl sm:text-3xl lg:text-5xl text-center text-white">Semakin cepat daftarnya, semakin<br/>BESAR Diskonnya!</h3>
	  	</div>
	  </div>
	  <div class="w-screen bg-white bg-cover bg-right-bottom bg-no-repeat bg-[url('./assets/img/asset-6.png')] md:overflow-hidden">
	  	<div class="content-wrapper py-10 md:pb-0 lg:py-0 px-5 sm:px-8 lg:px-20">
	  		<div class="flex flex-row">
	  			<div class="md:block flex-auto w-3/5 2xl:w-3/6">
	  				<div class="flex h-full flex-col justify-center">
	  					<div class="content text-sm sm:text-base md:text-sm lg:text-base pb-24 pt-16">
		  					<h4 class="mb-5 font-bold">Biaya apa saja di IIK Bhakta?</h4>
		  					<p>Biaya Kuliah di IIK Bhakta meliputi Sumbangan Pendidikan, DP3, SPP per semester, Biaya Pra Studi, Perlengkapan Praktikum, Biaya PKL, PBL dan Wisuda.</p>
		  					<h4 class="my-5 font-bold">Ketentuan pembayaran DP3</h4>
		  					<p>Biaya DP3 adalah Dana Partisipasi Penyelenggaraan Pendidikan yang memiliki dua metode pembayaran yaitu langsung dibayar lunas atau dicicil selama 10X untuk semua prodi dan 20x khusus bagi S1 Kedokteran Gigi. Biaya DP3 jika dibayar lunas saat DU, maka akan mendapat diskon langsung sebsar 10% dari jumlah D3 normal, tetapi bila dicicil maka IIK Bhakta memberi kemudahan dibayar awal saat DU dan dilanjutkan cicilan hingga 10x atau 20x per bulannya</p>
		  					<h4 class="my-5 font-bold">Informasi RPL</h4>
		  					<p>Jalur RPL (Rekognisi Pembelajaran Lampau) adalah program penyetaraan akademik dari pendidikan yang telah ditempuh sebelumnya dan telah dibuktikan dengan ijazah untuk memperoleh kualifikasi pendidikan lebih tinggi pada program studi IIK Bhakta tertentu. Ada 6 program studi yang ditawarkan program RPL di IIK Bhakta, diantaranya:</p>
		  					<ol class="list-decimal pl-5 mb-5">
		  						<li>D4 Teknologi Laboratorium Medis</li>
		  						<li>S1 Kesehatan Masyarakat</li>
		  						<li>S1 Keperawatan</li>
		  						<li>S1 Administrasi Rumah Sakit</li>
		  						<li>D4 Pengobatan Tradisional Tiongkok</li>
		  						<li>S1 Farmasi</li>
		  					</ol>
		  					<p>Info lebih detail mengenail RPL dapat langsung cek di halaman <a href="www.iik.ac.id/RPLProgsus">www.iik.ac.id/RPLProgsus</a></p>
		  				</div>
	  				</div>

	  			</div>
	  			<div class="hidden md:block flex-auto w-3/6">
	  				<div class="img-wrapper md:h-full md:flex md:flex-col md:items-end md:justify-end">
	  					<img class="w-full" src="./assets/img/asset-16.png" />
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	   <div class="w-screen px-10 lg:px-20 py-16 bg-slate-300 md:overflow-hidden">
	   		<h3 class="font-bold text-center sm:text-left text-lg sm:text-2xl mb-5">Simulasi Biaya Pendidikan S1 Kedokteran Gigi TA 2023/2024</h3>
	   		<div class="sm:border-2 sm:border-white">
	   			<div class="hidden sm:grid grid-cols-1 sm:grid-cols-4 content-center">
		   			<div class="text-white text-sm lg:text-base bg-green-100 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">Jenis Biaya</div>
		   			<div class="text-white text-sm lg:text-base bg-green-100 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</div>
		   			<div class="text-white text-sm lg:text-base bg-green-100 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">Jareg 2<br/>(06 Februari - 03 Juni 2023)</div>
		   			<div class="text-white text-sm lg:text-base bg-green-100 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</div>
		   		</div>
		   		<div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
		   			<div class="text-white sm:text-black sm:bg-green-50 2xs:bg-green-100 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex items-start flex-col items-center sm:items-start justify-center">DP3 Normal<br/>(Bisa dicicil hingga 40 kali tanpa DP)
					</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</span>
		   				<br/>Rp 300.000.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 50 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 2<br/>(06 Februari - 03 Juni 2023)</span>
		   				<br/>Rp 325.000.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 25 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</span>
		   				<br/>Rp 350.000.000
		   			</div>
		   		</div>
		   		<div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
		   			<div class="text-white sm:text-black sm:bg-green-50 2xs:bg-green-100 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex items-start flex-col items-center sm:items-start justify-center">DP3 Langsung Lunas<br/>(Hemat 50 Juta dari DP3 Normal)
					</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</span>
		   				<br/>Rp 250.000.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 50 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 2<br/>(06 Februari - 03 Juni 2023)</span>
		   				<br/>Rp 275.000.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 25 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</span>
		   				<br/>Rp 300.000.000
		   			</div>
		   		</div>
		   		<div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
		   			<div class="text-white sm:text-black sm:bg-green-50 2xs:bg-green-100 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex items-start flex-col items-center sm:items-start text-center sm:text-left justify-center">DP3 Cicilan 10x<br/>(Hemat 40 juta dari DP3 Normal)
					</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</span>
		   				<br/>DP3 Awal Rp 125.000.000
						<br/>Cicilan 10x Rp 11.250.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 2 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 2<br/>(06 Februari - 03 Juni 2023)</span>
		   				<br/>DP3 Awal Rp 137.500.000
						<br/>Cicilan 10x Rp 12.300.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 1 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</span>
		   				<br/>DP3 Awal Rp 150.000.000
						<br/>Cicilan 10x Rp 13.300.000
		   			</div>
		   		</div>
		   		<div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
		   			<div class="text-white sm:text-black sm:bg-green-50 2xs:bg-green-100 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex items-start flex-col items-center sm:items-start text-center sm:text-left justify-center">DP3 Cicilan 20x<br/>(Hemat 20 juta dari DP3 Normal)
					</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</span>
		   				<br/>DP3 Awal Rp 125.000.000
						<br/>Cicilan 20x Rp 7.750.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 1.2 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 2<br/>(06 Februari - 03 Juni 2023)</span>
		   				<br/>DP3 Awal Rp 137.500.000
						<br/>Cicilan 20x Rp 8.375.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 600 Ribu dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</span>
		   				<br/>DP3 Awal Rp 150.000.000
						<br/>Cicilan 20x Rp 9.000.000
		   			</div>
		   		</div>
		   		<div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
		   			<div class="text-white sm:text-black sm:bg-green-50 2xs:bg-green-100 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex items-start flex-col items-center sm:items-start text-center sm:text-left justify-center">DP3 Cicilan 40x<br/>(Hemat 10 juta dari DP3 Normal)
					</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 1<br/>(03 Oktober - 04 Februari 2023)</span>
		   				<br/>DP3 Awal Rp 125.000.000
						<br/>Cicilan 40x Rp 4.125.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 1.2 Juta dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 2<br/>(06 Februari - 03 Juni 2023)</span>
		   				<br/>DP3 Awal Rp 137.500.000
						<br/>Cicilan 40x Rp 4.750.000
		   				<br/><span class="text-red-900 font-bold text-sm">Hemat 600 Ribu dari Jareg 3</span>
		   			</div>
		   			<div class="bg-green-50 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
		   				<span class="block sm:hidden font-bold mb-3">Jareg 3<br/>(05 Juni - 26 Agustus 2023)</span>
		   				<br/>DP3 Awal Rp 137.500.000
						<br/>Cicilan 40x Rp 5.375.000
		   			</div>
		   		</div>
	   		</div>
	   </div>
	   <div class="w-screen px-5 sm:px-10 lg:px-20 py-16 bg-white md:overflow-hidden">
	   	<div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3  content-center">
	   		<div class="left-content">
	   			<div class="btn-wrapper text-center sm:border-r-2 sm:border-black">
	   				<a href="https://iik.ac.id/web.php?judul=kuis_penentu_prodi_iik_bhaktiwiyata&r=kuis_penentu_prodi" target="_blank" class="block mx-auto text-sm lg:text-base mb-5 border border-black font-bold px-10 lg:px-5 py-4 rounded-full sm:max-w-[85%]" >Kuis Penentuan Prodi Anda</a>
	   				<a href="https://drive.google.com/drive/u/0/folders/1Hv3HNhCpvvQbq8_3C5AX6BRZ0nQuT2Gd" class="block mx-auto text-sm lg:text-base border border-black font-bold px-2 md:px-3 py-4 rounded-full  sm:max-w-[85%]" >Download Brosur dan Welcome Book Prodi</a>
	   			</div>
	   		</div>
	   		<div class="center-content mt-5 sm:mt-0">
	   			<div class="btn-wrapper md:border-r-2 md:border-black text-center pb-[15px]">
	   				<h4 class="font-bold text-sm md:text-base">Admin Kami Siap Menjawab Pertanyaan Anda</h4>
	   				<a href="https://api.whatsapp.com/send/?phone=6285717171735&text&app_absent=0" class="block mx-auto border border-black px-5 lg:px-2 py-4 rounded-full mt-5 mb-5 sm:max-w-[90%] md:max-w-[85%]" >
	   					<img class="inline-block md:hidden lg:inline-block align-bottom" src="./assets/img/asset-17.png" /> <span class="inline-block"> Chat WA Admin Pendaftaran</span></a>
	   			</div>
	   		</div>
	   		<div class="right-content sm:col-span-2 md:col-span-1">
	   			<div class="content-wrapper md:pl-10 text-center md:text-left">
	   				<h4 class="font-bold inline-block md:block ">Link :</h4>
	   				<ul class="py-4 inline-block md:block pl-3 md:pl-0">
	   					<li class="text-lg inline-block mr-3 md:block md:mr-0">
	   						<a href="https://iik.ac.id/web.php?judul=info_beasiswa_iik_bhaktiwiyata&r=pendaftaran&sp=infobeasiswa" target="_blank" class="text-red-800"> Info Beasiswa</a>
	   					</li>
	   					<li class="text-lg inline-block mr-3 md:block md:mr-0">
	   						<a href="https://iik.ac.id/web.php?judul=tata_cara_daftar_iik_bhaktiwiyata&r=pendaftaran&sp=tatacaradaftar" target="_blank" class="text-red-800"> Tata Cara Pendaftaran</a>
	   					</li>
	   					<li class="text-lg inline-block mr-3 md:block md:mr-0">
	   						<a href="https://iik.ac.id/web.php?judul=info_fasilitas_dan_peta_kampus_iik_bhaktiwiyata&r=pendaftaran&sp=turkampus" target="_blank" class="text-red-800"> Virtual Tour Campus</a>
	   					</li>
	   				</ul>
	   			</div>
	   		</div>
	   	</div>
	   </div>
	   <div class="w-screen md:h-screen px-10 lg:px-20 py-16 bg-slate-300 md:overflow-hidden">
	   		<div class="content-wrapper h-full">
	   			<div class="flex h-full flex-col md:flex-row">
	   				<div class="md:block md:h-full md:flex-auto md:w-3/6">
	   					<div class="flex h-full flex-col justify-center">
	   						<div class="content text-sm  sm:text-sm lg:text-base pb-10 text-lg">
	   							<h3 class="font-bold text-lg sm:text-2xl mb-5">Sambutan Rektor</h3>
	   							<p class="mb-5 text-justify">Selamat datang di Institut Ilmu Kesehatan Bhakti Wiyata Kediri, institut kesehatan pertama di Indonesia. Kami sangat komit dengan harapan kami agar pendidikan tinggi kesehatan berkualitas dapat menjangkau berbagai kalangan dari segala penjuru Indonesia. Dengan demikian, harapannya tenaga kesehatan profesional berasal dari berbagai kalangan dan daerah, sehingga nanti kemajuan kesehatan Indonesia juga dapat merata di negeri ini. </p>
	   							<p class="mb-5 text-justify">Kami akan berusaha semaksimal mungkin menjadikan anak didik tenaga kesehatan yang andal dan profesional dengan memberikan pengetahuan kesehatan terkini, keterampilan dan kematangan pribadi yang kelak akan menjadi bekal dalam menjalankan profesinya di tengah masyarakat. Percayakan pendidikan berkualitas bersama kami IIK Bhakta!</p>
	   						</div>
	   					</div>
	   				</div>
	   				<div class="md:block md:flex-auto md:w-3/5">
	   					<div class="video-wrapper h-96 md:h-full md:pl-10">
	   						<iframe width="100%" height="100%" src="https://www.youtube.com/embed/7ktp65pjrk4?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   </div>
	   <div class="w-screen px-10 lg:px-20">
	   	<div class="content-wrapper py-14">
	   		<div class="title text-center">
	   			<h3 class="text-2xl lg:text-4xl font-bold">Partner Kerjasama Nasional</h3>
	   		</div>
	   		<div class="img-wrapper text-center py-10">
	   			<div class="grid grid-cols-3 sm:grid-cols-5 lg:grid-cols-8 gap-2 sm:justify-items-center">
	   				<?php for($i=1; $i <= 27; $i++){ ?>
			          <div class="partner-wrapper px-2 py-4">
			           	<img class="max-w-full" src="./assets/img/partner-nasional/partner-<?= $i; ?>.png" />
			          </div>
			        <?php } ?>
	   			</div>
	   		</div>
	   		<div class="title text-center">
	   			<h3 class="text-2xl lg:text-4xl font-bold">Partner Kerjasama Internasional</h3>
	   		</div>
	   		<div class="img-wrapper text-center py-10">
	   			<div class="grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-6 gap-2 justify-items-center">
	   				<?php for($i=1; $i <= 12; $i++){ ?>
			          <div class="partner-wrapper px-2 py-4">
			           	<img class="max-w-full" src="./assets/img/partner-internasional/partner-<?= $i; ?>.png" />
			          </div>
			        <?php } ?>
	   			</div>
	   		</div>
	   	</div>
	   </div>
  </section>
  <footer>
	  	<div class="w-screen px-5 sm:px-20 py-20 bg-blue-900 bg-cover bg-center bg-no-repeat bg-[url('./assets/img/footer-bg.jpg')]">
	  		<div class="grid grid-cols-6 gap-4">
	  			<div class="col-span-6 mb-4 md:mb-0 lg:col-span-2">
	  				<img class="w-[75%]" src="./assets/img/logo_white.png" />
	  			</div>
	  			<div class="footer-content col-span-6 mb-4 md:mb-0 md:col-span-2 lg:col-span-2">
	  				<h4 class="text-white font-bold text-xl">Site Traffic</h4>
	  				<div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-3">
	  					<div class="flex-auto">
	  						<img class="w-auto" src="./assets/img/24hrs_white.png" />
	  					</div>
	  					<div class="flex-auto">
	  						<span class="text-sm text-white" >Last 24 hrs</span>
	  					</div>
	  					<div class="flex-none">
	  						<div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2"><?= number_format($onFirst); ?></div>
	  					</div>
	  				</div>
	  				<div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
	  					<div class="flex-auto">
	  						<img class="w-auto" src="./assets/img/7days_white.png" />
	  					</div>
	  					<div class="flex-auto">
	  						<span class="text-sm text-white" >Last 7 days</span>
	  					</div>
	  					<div class="flex-none">
	  						<div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2"><?= number_format($onSeven); ?></div>
	  					</div>
	  				</div>
	  				<div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
	  					<div class="flex-auto">
	  						<img class="w-auto" src="./assets/img/30days_white.png" />
	  					</div>
	  					<div class="flex-auto">
	  						<span class="text-sm text-white" >Last 30 days</span>
	  					</div>
	  					<div class="flex-none">
	  						<div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2"><?= number_format($onthree); ?></div>
	  					</div>
	  				</div>
	  				<div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
	  					<div class="flex-auto">
	  						<span class="text-base text-white font-bold" >Online Now</span>
	  					</div>
	  					<div class="flex-none">
	  						<div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2"><?= number_format($online); ?></div>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="footer-content col-span-6 md:col-span-4 lg:col-span-2">
	  				<h4 class="text-white font-bold text-lg sm:text-xl">Institut Ilmu Kesehatan Bhakti Wiyata</h4>
	  				<div class="grid grid-cols-3 gap-2">
	  					<div class="contact-wrapper col-span-3 sm:col-span-2 mt-3">
	  						<div class="flex items-center">
	  							<div class="flex-none pr-2">
			  						<img class="w-auto" src="./assets/img/marker_white.png" />
			  					</div>
			  					<div class="flex-auto pl-2 border-l-2 border-white">
			  						<span class="text-base text-white" >Jl. KH. Wahid Hasyim 65<br/>Kediri 64114 Jawa Timur</span>
			  					</div>
	  						</div>
	  						<div class="flex items-center mt-2">
	  							<div class="flex-none pr-2">
			  						<img class="w-3" src="./assets/img/phone_white.png" />
			  					</div>
			  					<div class="flex-auto pl-2 border-l-2 border-white">
			  						<span class="text-base text-white" >Tlp. 0354 773299 / 773535</span>
			  					</div>
	  						</div>
	  						<div class="flex items-center mt-2">
	  							<div class="flex-none pr-2">
			  						<img class="w-3" src="./assets/img/fax_white.png" />
			  					</div>
			  					<div class="flex-auto pl-2 border-l-2 border-white">
			  						<span class="text-base text-white" >Fax. 0354 721539</span>
			  					</div>
	  						</div>
	  						<div class="flex items-center mt-2">
	  							<div class="flex-none pr-2">
			  						<img class="w-3" src="./assets/img/wa_white.png" />
			  					</div>
			  					<div class="flex-auto pl-2 border-l-2 border-white">
			  						<span class="text-base text-white" >Whatsapp 0857 1717 1735</span>
			  					</div>
	  						</div>
	  					</div>
	  					<div class="map-wrapper h-full flex flex-col col-span-3 sm:col-span-1 justify-center items-center pt-2">
	  						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d538.8116192820244!2d112.00567632095293!3d-7.81738692213297!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xafc394ffd8990c4d!2sInstitut%20Ilmu%20Kesehatan%20Bhakti%20Wiyata!5e0!3m2!1sid!2sid!4v1640838293695!5m2!1sid!2sid" width="100%" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	  					</div>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  <div class="bg-blue-900 px-20 py-5">
	  	<div class="grid grid-cols-2">
	  		<div class="sosmed-wrapper col-span-2 sm:col-span-1">
	  			<div class="flex justify-center sm:justify-start">
	  				<a href="http://www.instagram.com/iikbhaktiwiyata/" class="mr-8">
	  					<img class="w-auto" src="./assets/img/ig_white.png" />
	  				</a>
	  				<a href="http://www.facebook.com/iikbwkediri" class="mr-8">
	  					<img class="w-auto" src="./assets/img/fb_white.png" />
	  				</a>
	  				<a href="http://www.youtube.com/channel/UCWWpX0nV9b4fNPQt5dNWy5w" class="mr-8">
	  					<img class="w-auto" src="./assets/img/yt_white.png" />
	  				</a>
	  				<a href="https://iik.ac.id/#" class="mr-8">
	  					<img class="w-auto" src="./assets/img/twitter_white.png" />
	  				</a>
	  			</div>
	  		</div>
	  		<div class="copy-wrapper text-white text-center col-span-2 mt-4 sm:mt-0 sm:col-span-1">
	  			<span>&copy;</span> <a href="https://iik.ac.id/">IIK Bhakti Wiyata</a>
	  		</div>
	  	</div>
	  </div>
  </footer>
  <div class="fix-wrapper z-10 fixed right-0 bottom-0 p-5">
  	<a href="https://api.whatsapp.com/send/?phone=6285717171735&text&app_absent=0" class="block">
  		<img class="w-40 sm:w-64" src="./assets/img/wa-btn.png" />
  	</a>
  	<a href="https://iik.ac.id/devweb2/home/indexin.php?opt=olreg" class="mt-2 block">
  		<img class="w-40 sm:w-64" src="./assets/img/daftar-btn.png" />
  	</a>
  </div>
</body>
<script>
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 1,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        500: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        960: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
      },
    });
  </script>
</html>